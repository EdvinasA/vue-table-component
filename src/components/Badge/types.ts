export enum Status {
  Active = 'active',
  Valid = 'valid',
  Canceled = 'canceled',
  Expired = 'expired',
  InActive = 'inactive',
}
