interface SubgroupTableData {
  fullname?: string;
  code?: string;
  expiration?: string;
  status?: string;
}

export interface SubgroupTableRows {
  id?: number;
  data?: SubgroupTableData[];
}

export interface GroupCheckboxesChecked {
  group_id: number;
  subgroups: number[];
}
