import { PaginationButtonAction, PaginationButtons } from './types';
import ArrowLeft from '../../../../assets/arrow-leftt.svg';
import ArrowRight from '../../../../assets/arrow-rightt.svg';

export const paginationButtons: PaginationButtons[] = [
  { icon: ArrowLeft, action: PaginationButtonAction.Backwards },
  { icon: ArrowRight, action: PaginationButtonAction.Forwards },
];
