export enum PaginationButtonAction {
  Backwards = 'backwards',
  Forwards = 'forwards',
}

export interface PaginationButtons {
  icon: string;
  action: PaginationButtonAction;
}
