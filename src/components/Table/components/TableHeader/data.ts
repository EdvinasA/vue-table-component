import { TableHeaderSortButtons, SortActions } from './types';
import TriangleUp from '../../../../assets/triangle-up.svg';
import TriangleDown from '../../../../assets/triangle-down.svg';

export const tableHeadersSortButtons: TableHeaderSortButtons[] = [
  { icon: TriangleUp, action: SortActions.ASC },
  { icon: TriangleDown, action: SortActions.DESC },
];
