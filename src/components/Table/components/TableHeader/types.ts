export enum SortActions {
  ASC = 'ASC',
  DESC = 'DESC',
}

export interface TableHeaderSortButtons {
  icon: string;
  action: SortActions;
}
