export interface TableColumns {
  key: string;
  name: string;
  sortType: string;
}
export interface SortArguments {
  order: string;
  sortBy: string;
}

export interface TableData {
  id: number;
  fullname: string;
  department: string;
  userStatus: string;
  jobTitle: string;
  mental_health_expiration: string;
  mental_health_status: string;
  physical_health_expiration: string;
  physical_health_status: string;
  computer_work_expiration: string;
  computer_work_status: string;
}

export type SortableTableData = Omit<TableData, 'id'>;
